import tensorflow as tf


def train(mnist, sess, input, output, train_step, cross_entropy, accuracy):
    for epoch in range(10):
        batch_xs, batch_ys = mnist.train.next_batch(1000)
        sess.run(train_step, feed_dict={input: batch_xs, output: batch_ys})
        if (epoch % 100 == 0):
            train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy)
            print("train : {0}".format(sess.run(accuracy, feed_dict={input: batch_xs, output: batch_ys})))
            print("test : {0}".format(sess.run(accuracy, feed_dict={input: mnist.test.images, output: mnist.test.labels})))
