import numpy
import tensorflow as tf

from ImageRecognition.helper_nn import train
from ImageRecognition.simple_nn import init_nn as init_simple_nn
from ImageRecognition.convolutional_nn import init_nn as init_convolutional_nn
from tensorflow.examples.tutorials.mnist import input_data

No = 10
N0 = 784
conv = False
auto_one_hot = No == 10



mnist = input_data.read_data_sets("MNIST_data/letters", one_hot=auto_one_hot, reshape=False)


def dense_to_one_hot(labels_dense, num_classes):
  """Convert class labels from scalars to one-hot vectors."""
  num_labels = labels_dense.shape[0]
  index_offset = numpy.arange(num_labels) * num_classes
  labels_one_hot = numpy.zeros((num_labels, num_classes))
  labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
  return labels_one_hot

if(not auto_one_hot):
    mnist[0]._labels = dense_to_one_hot(mnist[0]._labels, No)
    mnist[1]._labels = dense_to_one_hot(mnist[1]._labels, No)
    mnist[2]._labels = dense_to_one_hot(mnist[2]._labels, No)

sess = tf.InteractiveSession()

#input layer
x = tf.placeholder(tf.float32, [None, 28, 28, 1])

if conv:
    N2, y2 = init_convolutional_nn(x, 28, 28)
else:
    N2, y2 = init_simple_nn(tf.reshape(x, [-1, 28*28]), N0)

#output layer weight + biais
W = tf.Variable(tf.truncated_normal([N2, No], stddev=0.1))
b = tf.Variable(tf.zeros([No]))

#output layer
y = tf.nn.softmax(tf.matmul(y2, W) + b)

#known results
y_ = tf.placeholder(tf.float32, [None, No])

#initialize weights
tf.global_variables_initializer().run()

#check difference between right ouput and result given
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

#backward propagation
train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy)

correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

train(mnist, sess, x, y_, train_step, cross_entropy, accuracy)

result = sess.run(y, feed_dict={x: mnist.validation.images})
print(' '.join(map(str, result)))