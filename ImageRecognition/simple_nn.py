import tensorflow as tf

N1 = 100
N2 = 30

def init_nn(input, size):
    # layer 1  layer weight + biais
    W1 = tf.Variable(tf.truncated_normal([size, N1], stddev=0.1))
    b1 = tf.Variable(tf.zeros([N1]))

    # layer 1 + dropout
    y1f = tf.nn.relu(tf.matmul(input, W1) + b1)
    y1 = tf.nn.dropout(y1f, 0.75)

    # layer 2  layer weight + biais
    W2 = tf.Variable(tf.truncated_normal([N1, N2], stddev=0.1))
    b2 = tf.Variable(tf.zeros([N2]))

    # layer 2 + dropout
    y2f = tf.nn.relu(tf.matmul(y1, W2) + b2)
    return N2, tf.nn.dropout(y2f, 0.75)
