import tensorflow as tf

S1 = 1
S2 = 2
S3 = 2

N = 500


def init_nn(i, size_v, size_h, patch_size=8, information_number=1, first_output_number=4):
    W1 = tf.Variable(tf.truncated_normal([patch_size, patch_size, information_number, first_output_number], stddev=0.1))
    B1 = tf.Variable(tf.ones([first_output_number]) / 10)
    Y1 = tf.nn.relu(tf.nn.conv2d(i, W1, strides=[1, S1, S1, 1], padding="SAME") + B1)

    v_size = size_v / S1
    h_size = size_h / S1

    W2 = tf.Variable(tf.truncated_normal([patch_size - 1, patch_size - 1, first_output_number, first_output_number * 2], stddev=0.1))
    B2 = tf.Variable(tf.ones([first_output_number * 2]) / 10)
    Y2 = tf.nn.relu(tf.nn.conv2d(Y1, W2, strides=[1, S2, S2, 1], padding="SAME") + B2)

    v_size = v_size / S2
    h_size = h_size / S2

    W3 = tf.Variable(tf.truncated_normal([patch_size - 2, patch_size - 2,  first_output_number * 2,  first_output_number * 2 * 2], stddev=0.1))
    B3 = tf.Variable(tf.ones([first_output_number * 2 * 2]) / 10)
    Y3 = tf.nn.relu(tf.nn.conv2d(Y2, W3, strides=[1, S3, S3, 1], padding="SAME") + B3)

    v_size = v_size / S3
    h_size = h_size / S3

    YY = tf.reshape(Y3, shape=[-1, int(v_size) * int(h_size) * first_output_number * 2 * 2])

    W4 = tf.Variable(tf.truncated_normal([int(v_size) * int(h_size) * first_output_number * 2 * 2, N], stddev=0.1))
    B4 = tf.Variable(tf.ones([N]) / 10)

    Yf = tf.nn.relu(tf.matmul(YY, W4) + B4)

    return N, tf.nn.dropout(Yf, 0.90)

