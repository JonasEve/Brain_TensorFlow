import tensorflow as tf
import tensorflow.contrib as contrib
import numpy as np
import os
import LanguageRecognition.my_txtutils as txt

ALPHASIZE = 98
CELLSIZE = 512
NLAYERS = 3
SEQLEN = 30
BATCHSIZE = 100

# load data, either shakespeare, or the Python source of Tensorflow itself
shakedir = "shakespeare/*.txt"
# shakedir = "../tensorflow/**/*.py"
codetext, valitext, bookranges = txt.read_data_files(shakedir, validation=True)

sess = tf.InteractiveSession()

# display some stats on the data
epoch_size = len(codetext) // (BATCHSIZE * SEQLEN)
txt.print_data_stats(len(codetext), len(valitext), epoch_size)
batchsize = tf.placeholder(tf.int32)

X = tf.placeholder(tf.uint8, [None, None], name="X")
Xo = tf.one_hot(X, ALPHASIZE, 1.0, 0.0)

Y_ = tf.placeholder(tf.uint8, [None, None], name="Y")
Yo_ = tf.one_hot(Y_, ALPHASIZE, 1.0, 0.0)

Hin = tf.placeholder(tf.float32, [None, CELLSIZE * NLAYERS], name="Hin")

#the model
mCell = contrib.rnn.MultiRNNCell([contrib.rnn.GRUCell(CELLSIZE) for _ in range(NLAYERS)], state_is_tuple=False)
Yr, H = tf.nn.dynamic_rnn(mCell, Xo, dtype=tf.float32, initial_state=Hin)

#output layer
Yflat = tf.reshape(Yr, [-1, CELLSIZE])
Ylogits = tf.contrib.layers.linear(Yflat, ALPHASIZE)
Yflat_ = tf.reshape(Yo_, [-1, ALPHASIZE])
Yo = tf.nn.softmax(Ylogits)
Y = tf.arg_max(Yo, 1)
Y = tf.reshape(Y, [BATCHSIZE, -1])

loss = tf.nn.softmax_cross_entropy_with_logits(logits=Ylogits, labels=Yflat_)
loss = tf.reshape(loss, [BATCHSIZE, -1])
train_step = tf.train.AdamOptimizer(1e-3).minimize(loss)

step = 0
istate = np.zeros([BATCHSIZE, CELLSIZE*NLAYERS])

saver = tf.train.Saver()

if not os.path.exists("checkpoints"):
    os.mkdir("checkpoints")
    init = tf.global_variables_initializer()
    sess.run(init)
else:
    saver.restore(sess, "checkpoints/rnn_train.chk")


# stats for display
seqloss = tf.reduce_mean(loss, 1)
batchloss = tf.reduce_mean(seqloss)
accuracy = tf.reduce_mean(tf.cast(tf.equal(Y_, tf.cast(Y, tf.uint8)), tf.float32))
loss_summary = tf.summary.scalar("batch_loss", batchloss)
acc_summary = tf.summary.scalar("batch_accuracy", accuracy)
summaries = tf.summary.merge([loss_summary, acc_summary])


for x, y_, epoch in txt.rnn_minibatch_sequencer(codetext, BATCHSIZE, SEQLEN, 1000):
    feed_dict = {X: x, Y_: y_, Hin: istate, batchsize: BATCHSIZE}
    _,y,outh = sess.run([train_step, Y, H], feed_dict=feed_dict)

    if(step % 300000 == 0):

        saver.save(sess, 'checkpoints/rnn_train.chk')

        y, l, bl, acc = sess.run([Y, seqloss, batchloss, accuracy], feed_dict=feed_dict)
        txt.print_learning_learned_comparison(x, y, l, bookranges, bl, acc, epoch_size, step, epoch)

        if(acc > 0.5):
            txt.print_text_generation_header()
            ry = np.array([[txt.convert_from_alphabet(ord("K"))]])
            rh = np.zeros([1, CELLSIZE * NLAYERS])
            for k in range(1000):
                ryo, rh = sess.run([Yo, H], feed_dict={X: ry, Hin: rh, batchsize: 1})
                rc = txt.sample_from_probabilities(ryo, topn=10 if epoch <= 1 else 2)
                print(chr(txt.convert_to_alphabet(rc)), end="")
                ry = np.array([[rc]])
        txt.print_text_generation_footer()

    istate = outh
    step += BATCHSIZE * SEQLEN