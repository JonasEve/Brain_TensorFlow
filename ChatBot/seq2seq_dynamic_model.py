import numpy as np #matrix math
import tensorflow as tf #machine learningt
from nltk.compat import raw_input

import ChatBot.helpers as helpers #for formatting data into batches and generating random sequence data
from tensorflow.contrib.rnn import LSTMCell, LSTMStateTuple
import tensorflow.contrib.rnn as rnn
import sys
#import LanguageRecognition.my_txtutils as txt

class Seq2SeqDynamicModel(object):

    def __init__(self, vocab_size, input_embedding_size, encoder_hidden_units, nb_layers):
        self.PAD = 0
        self.EOS = 1

        self.vocab_size = vocab_size
        self.input_embedding_size = input_embedding_size

        self.encoder_hidden_units = encoder_hidden_units
        self.nb_layers_input = nb_layers
        self.decoder_hidden_units = encoder_hidden_units * 2
        self.nb_layers_output = nb_layers

        self.encoder_inputs = tf.placeholder(shape=[None, None], dtype=tf.int32, name='encoder_inputs')
        self.encoder_inputs_length = tf.placeholder(shape=[None,], dtype=tf.int32, name='encoder_inputs_length')
        self.decoder_targets = tf.placeholder(shape=[None, None], dtype=tf.int32, name='decoder_targets')

        embeddings = tf.Variable(tf.random_uniform([vocab_size, input_embedding_size], -1.0, 1.0), dtype=tf.float32)

        encoder_inputs_embedded = tf.nn.embedding_lookup(embeddings, self.encoder_inputs)

        #<editor-fold desc="Encoder">

        ((encoder_fw_outputs,
        encoder_bw_outputs),
        (encoder_fw_final_state,
        encoder_bw_final_state)) = (
            tf.nn.bidirectional_dynamic_rnn(cell_fw=rnn.DropoutWrapper(
                                                        rnn.MultiRNNCell([rnn.DropoutWrapper(LSTMCell(self.encoder_hidden_units, state_is_tuple=True), 0.75) for _ in range(self.nb_layers_input)],
                                                            state_is_tuple=True), 0.75),
                                    cell_bw=rnn.DropoutWrapper(
                                                rnn.MultiRNNCell([rnn.DropoutWrapper(LSTMCell(self.encoder_hidden_units, state_is_tuple=True), 0.75) for _ in range(self.nb_layers_input)],
                                                    state_is_tuple=True), 0.75),
                                    inputs=encoder_inputs_embedded,
                                    sequence_length=self.encoder_inputs_length,
                                    dtype=tf.float32, time_major=True)
        )



        encoder_outputs = tf.concat((encoder_fw_outputs, encoder_bw_outputs), 2)

        encoder_final_state= ()

        for i in range(len(encoder_fw_final_state)):
            encoder_final_state_c = tf.concat(
                (encoder_fw_final_state[i].c, encoder_bw_final_state[i].c), 1)
            encoder_final_state_h = tf.concat(
                (encoder_fw_final_state[i].h, encoder_bw_final_state[i].h), 1)
            encoder_final_state = encoder_final_state + (LSTMStateTuple(
                                                            c=encoder_final_state_c,
                                                            h=encoder_final_state_h
                                                            ),
                                                        )

        #</editor-fold>

        #<editor-fold desc="Decoder">


        decoder_cell = rnn.DropoutWrapper(
            rnn.MultiRNNCell([rnn.DropoutWrapper(LSTMCell(self.decoder_hidden_units, state_is_tuple=True), 0.75) for _ in range(self.nb_layers_output)],
                             state_is_tuple=True), 0.75)

        encoder_max_time, batch_size = tf.unstack(tf.shape(self.encoder_inputs))

        self.decoder_lengths = self.encoder_inputs_length + 1

        W = tf.Variable(tf.random_uniform([self.decoder_hidden_units, vocab_size], -1, 1), dtype=tf.float32)

        b = tf.Variable(tf.zeros([vocab_size]), dtype=tf.float32)

        eos_time_slice = tf.ones([batch_size], dtype=tf.int32, name='EOS')
        pad_time_slice = tf.zeros([batch_size], dtype=tf.int32, name='PAD')

        eos_step_embedded = tf.nn.embedding_lookup(embeddings, eos_time_slice)
        pad_step_embedded = tf.nn.embedding_lookup(embeddings, pad_time_slice)

        def loop_fn_initial():
            initial_elements_finished = (0 >= self.decoder_lengths)  # all False at the initial step
            #end of sentence
            initial_input = eos_step_embedded
            #last time steps cell state
            initial_cell_state = encoder_final_state
            #none
            initial_cell_output = None
            #none
            initial_loop_state = None  # we don't need to pass any additional information
            return (initial_elements_finished,
                    initial_input,
                    initial_cell_state,
                    initial_cell_output,
                    initial_loop_state)

        def loop_fn_transition(time, previous_output, previous_state, previous_loop_state):
            def get_next_input():
                # dot product between previous ouput and weights, then + biases
                output_logits = tf.add(tf.matmul(previous_output, W), b)
                # Logits simply means that the function operates on the unscaled output of
                # earlier layers and that the relative scale to understand the units is linear.
                # It means, in particular, the sum of the inputs may not equal 1, that the values are not probabilities
                # (you might have an input of 5).
                # prediction value at current time step

                # Returns the index with the largest value across axes of a tensor.
                prediction = tf.argmax(output_logits, axis=1)
                # embed prediction for the next input
                next_input = tf.nn.embedding_lookup(embeddings, prediction)
                return next_input

            elements_finished = (time >= self.decoder_lengths)  # this operation produces boolean tensor of [batch_size]
            # defining if corresponding sequence has ended



            # Computes the "logical and" of elements across dimensions of a tensor.
            finished = tf.reduce_all(elements_finished)  # -> boolean scalar
            # Return either fn1() or fn2() based on the boolean predicate pred.
            input = tf.cond(finished, lambda: pad_step_embedded, get_next_input)

            # set previous to current
            state = previous_state
            output = previous_output
            loop_state = None

            return (elements_finished,
                    input,
                    state,
                    output,
                    loop_state)

        def loop_fn(time, previous_output, previous_state, previous_loop_state):
            if previous_state is None:    # time == 0
                assert previous_output is None and previous_state is None
                return loop_fn_initial()
            else:
                return loop_fn_transition(time, previous_output, previous_state, previous_loop_state)


        decoder_outputs_ta, decoder_final_state, _ = tf.nn.raw_rnn(decoder_cell, loop_fn)
        decoder_outputs = decoder_outputs_ta.stack()

        decoder_max_steps, decoder_batch_size, decoder_dim = tf.unstack(tf.shape(decoder_outputs))

        decoder_outputs_flat = tf.reshape(decoder_outputs, (-1, decoder_dim))
        decoder_logits_flat = tf.add(tf.matmul(decoder_outputs_flat, W), b)
        decoder_logits = tf.reshape(decoder_logits_flat, (decoder_max_steps, decoder_batch_size, vocab_size))

        self.Yo = tf.nn.softmax(decoder_logits)

        self.decoder_prediction = tf.argmax(decoder_logits, 2)

        #</editor-fold>

        stepwise_cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
            labels=tf.one_hot(self.decoder_targets, depth=vocab_size, dtype=tf.float32),
            logits=decoder_logits,
        )

        self.loss = tf.reduce_mean(stepwise_cross_entropy)
        self.train_op = tf.train.AdamOptimizer().minimize(self.loss)

def main():

    tf.reset_default_graph()  # Clears the default graph stack and resets the global default graph.
    sess = tf.InteractiveSession()  # initializes a tensorflow session

    print(tf.__version__)

    vocab_size = 98
    input_embedding_size = 100  # character length

    encoder_hidden_units = 50  # num neurons

    model = Seq2SeqDynamicModel(vocab_size, input_embedding_size, encoder_hidden_units, 2)

    sess.run(tf.global_variables_initializer())

    batch_size = 10

    batches = helpers.random_letters_in(batch_size)
    batches_out = helpers.random_letters_out(batch_size)

    batches_test = helpers.random_letters_in(1)
    batches__test_out = helpers.random_letters_out(1)

    print('head of the batch:')
    for seq in next(batches)[:10]:
        print(helpers.decode_to_text(seq))


    def next_feed():
        batch = next(batches)
        batch_out = next (batches_out)
        encoder_inputs_, encoder_input_lengths_ = helpers.batch(batch)
        decoder_targets_, _ = helpers.batch(
            [(sequence) + [model.EOS] for sequence in batch_out],  max(encoder_input_lengths_) + 1
        )
        return {
            model.encoder_inputs: encoder_inputs_,
            model.encoder_inputs_length: encoder_input_lengths_,
            model.decoder_targets: decoder_targets_,
        }

    def next_feed_test():
        batch = next(batches_test)
        batch_out = next (batches__test_out)
        encoder_inputs_, encoder_input_lengths_ = helpers.batch(batch)
        decoder_targets_, _ = helpers.batch(
            [(sequence) + [model.EOS] for sequence in batch_out],  max(encoder_input_lengths_) + 1
        )
        return {
            model.encoder_inputs: encoder_inputs_,
            model.encoder_inputs_length: encoder_input_lengths_,
            model.decoder_targets: decoder_targets_,
        }

    loss_track = []

    max_batches = 1000
    batches_in_epoch = 100

    try:
        for batch in range(max_batches):
            fd = next_feed()
            _, l = sess.run([model.train_op, model.loss], fd)
            loss_track.append(l)

            if batch == 0 or batch % batches_in_epoch == 0:
                fd = next_feed_test()
                print('batch {}'.format(batch))
                print('  minibatch loss: {}'.format(sess.run(model.loss, fd)))

                predict_ = sess.run(model.decoder_prediction, fd)
                predict = []
                for word in sess.run(model.Yo, fd):
                    for batch in word :
                        val = helpers.sample_from_probabilities(batch, 2)
                        predict.append(val)

                for i, (inp, pred) in enumerate(zip(fd[model.encoder_inputs].T, predict_.T)):
                    print('  sample {}:'.format(i + 1))
                    print('    input     > {}'.format(helpers.decode_to_text(inp)))
                    print('    predicted > {}'.format(helpers.decode_to_text(pred)))
                    print('    proba > {}'.format(helpers.decode_to_text(predict)))
                    if i >= 2:
                        break
                print()

        while True:
            value = raw_input(">>>")

            encoder_inputs_, encoder_input_lengths_ = helpers.batch([helpers.encode_text(value)])

            fd = {model.encoder_inputs: encoder_inputs_,
                  model.encoder_inputs_length: encoder_input_lengths_}
            predict_ = sess.run(model.decoder_prediction, fd)
            predict = []
            for word in sess.run(model.Yo, fd):
                for batch in word:
                    val = helpers.sample_from_probabilities(batch, 2)
                    predict.append(val)

            for i, (inp, pred) in enumerate(zip(fd[model.encoder_inputs].T, predict_.T)):
                print('  sample {}:'.format(i + 1))
                print('    input     > {}'.format(helpers.decode_to_text(inp)))
                print('    predicted > {}'.format(helpers.decode_to_text(pred)))
                print('    proba > {}'.format(helpers.decode_to_text(predict)))
                if i >= 2:
                    break
            print()

    except KeyboardInterrupt:
        print('training interrupted')




if __name__ == '__main__':
    main()