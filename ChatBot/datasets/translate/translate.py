import numpy as np
import tensorflow as tf

from ChatBot import seq2seq_static_model
from ChatBot.datasets.translate import data_utils_translation


seq_lenght = 10
out_seq_lenght = 10
batch_size = 32

vocab_size = 4000
embedding_dim = 512

nb_layers = 2

_buckets = [(5, 10), (10, 15), (20, 25), (40, 50)]

en_train, fr_train, en_dev, fr_dev, _, _ = data_utils_translation.prepare_wmt_data(
      "/tmp", vocab_size, vocab_size)

def read_data(source_path, target_path, max_size=None):
  """Read data from source and target files and put into buckets.
  Args:
    source_path: path to the files with token-ids for the source language.
    target_path: path to the file with token-ids for the target language;
      it must be aligned with the source file: n-th line contains the desired
      output for n-th line from the source_path.
    max_size: maximum number of lines to read, all other will be ignored;
      if 0 or None, data files will be read completely (no limit).
  Returns:
    data_set: a list of length len(_buckets); data_set[n] contains a list of
      (source, target) pairs read from the provided data files that fit
      into the n-th bucket, i.e., such that len(source) < _buckets[n][0] and
      len(target) < _buckets[n][1]; source and target are lists of token-ids.
  """
  data_set = [[] for _ in _buckets]
  with tf.gfile.GFile(source_path, mode="r") as source_file:
    with tf.gfile.GFile(target_path, mode="r") as target_file:
      source, target = source_file.readline(), target_file.readline()
      counter = 0
      while source and target and (not max_size or counter < max_size):
        counter += 1
        if counter % 100000 == 0:
          print("  reading data line %d" % counter)
          tf.sys.stdout.flush()
        source_ids = [int(x) for x in source.split()]
        target_ids = [int(x) for x in target.split()]
        target_ids.append(data_utils_translation.EOS_ID)
        for bucket_id, (source_size, target_size) in enumerate(_buckets):
          if len(source_ids) < source_size and len(target_ids) < target_size:
            data_set[bucket_id].append([source_ids, target_ids])
            break
        source, target = source_file.readline(), target_file.readline()
  return data_set


load = False
save = False

model = seq2seq_static_model.Seq2SeqModel(_buckets, vocab_size, embedding_dim, nb_layers)

sess = tf.InteractiveSession()

if(load):
    model.load(sess)
else:
    sess.run(tf.global_variables_initializer())


dev_set = read_data(en_dev, fr_dev)
train_set = read_data(en_train, fr_train, 0)

train_bucket_sizes = [len(train_set[b]) for b in range(len(_buckets))]
train_total_size = float(sum(train_bucket_sizes))

train_buckets_scale = [sum(train_bucket_sizes[:i + 1]) / train_total_size
                           for i in range(len(train_bucket_sizes))]

for i in range(10):
    random_number_01 = np.random.random_sample()
    bucket_id = min([i for i in range(len(train_buckets_scale))
                     if train_buckets_scale[i] > random_number_01])

    encoder_size, decoder_size = _buckets[bucket_id]

    encoder_inputs, decoder_inputs, _ = data_utils_translation.get_batch(train_set, encoder_size, decoder_size, batch_size)

    feed_dict = {model.enc_inp[t] : encoder_inputs[t] for t in range (seq_lenght)}
    feed_dict.update({model.labels[i] : decoder_inputs[i] for i in range(out_seq_lenght)})

    sess.run(model.trainer, feed_dict)

random_number_01 = np.random.random_sample()
bucket_id = min([i for i in range(len(train_buckets_scale))
                     if train_buckets_scale[i] > random_number_01])

encoder_size, decoder_size = _buckets[bucket_id]

encoder_inputs, decoder_inputs, _ = data_utils_translation.get_batch(train_set, encoder_size, decoder_size, batch_size)

feed_dict = {model.enc_inp[t] : encoder_inputs[t] for t in range (seq_lenght)}

out = sess.run(model.dec_outputs, feed_dict=feed_dict)
dec_op_v = np.array(out).transpose([1, 0, 2])
dec_op = np.argmax(dec_op_v, axis=2)

if(save):
    model.save(sess)
