import numpy as np
import tensorflow as tf

from ChatBot import seq2seq_static_model
from ChatBot.datasets.twitter import data, data_utils
from ChatBot.seq2seq_dynamic_model import Seq2SeqDynamicModel

metadata, idx_q, idx_a = data.load_data()
(trainX, trainY), (testX, testY), (validX, validY) = data_utils.split_dataset(idx_q, idx_a)

seq_lenght = trainX.shape[-1]
out_seq_lenght = trainY.shape[-1]
batch_size = 32

vocab_size = len(metadata['idx2w'])
embedding_dim = 512

nb_layers = 2

load = False
save = False

#model = seq2seq_static_model.Seq2SeqModel([(seq_lenght, out_seq_lenght)], vocab_size, embedding_dim, nb_layers)
model = Seq2SeqDynamicModel(vocab_size, embedding_dim, embedding_dim)

sess = tf.InteractiveSession()

if(load):
    model.load(sess)
else:
    sess.run(tf.global_variables_initializer())

val_batch_gen = data_utils.rand_batch_gen(validX, validY, 32)
train_batch_gen = data_utils.rand_batch_gen(trainX, trainY, 1)

for i in range(10):
    batchX, batchY = train_batch_gen.__next__()

    feed_dict = {model.encoder_inputs : trainX,model.decoder_targets: trainY[i], model.encoder_inputs_length : [len(trainX[i])] }

    sess.run(model.train_op, feed_dict)

test_batch_gen = data_utils.rand_batch_gen(testX, testY, 3)
X = test_batch_gen.__next__()[0]

for index in range(len(testX[0])):
    decoded_x = data_utils.decode(sequence=X[index], lookup=metadata['idx2w'], separator=' ')
    print(decoded_x)

feed_dict = {model.encoder_inputs : X, model.encoder_inputs_length : [len(X)] }

out = sess.run(model.decoder_prediction, feed_dict=feed_dict)
#dec_op_v = np.array(out).transpose([1, 0, 2])
#dec_op = np.argmax(dec_op_v, axis=2)


for index in range(len(out)):
    decoded = data_utils.decode(sequence=out[index], lookup=metadata['idx2w'], separator=' ')
    print(decoded)

if(save):
    model.save(sess)
