import os
from pathlib import Path

import tensorflow as tf
import tensorflow.contrib.rnn as rnn
import tensorflow.contrib.legacy_seq2seq as seq2seq


class Seq2SeqModel(object):

    def __init__(self, buckets, vocab_size, embedding_dim, nb_layers):


        self.buckets = buckets

        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim

        self.nb_layers = nb_layers

        self.enc_inp = [tf.placeholder(shape=[None, ], dtype=tf.int64, name="inp_{}".format(t)) for t in range(buckets[-1][0])]
        self.labels = [tf.placeholder(shape=[None, ], dtype=tf.int64, name="labels_{}".format(t)) for t in
                  range(buckets[-1][1])]

        self.weights = [tf.ones_like(labels_t, dtype=tf.float32) for labels_t in self.labels]

        # Decoder input: prepend some "GO" token and drop the final
        # token of the encoder input

        self.dec_inp = [ tf.zeros_like(self.enc_inp[0], dtype=tf.int64, name='GO') ] + self.labels[:-1]

        #self.dec_inp = [self.labels[i + 1] for i in range(len(self.labels) - 1)]

        mcell = rnn.DropoutWrapper(
            rnn.MultiRNNCell([rnn.DropoutWrapper(rnn.GRUCell(embedding_dim), 0.75) for _ in range(nb_layers)],
                             state_is_tuple=True), 0.75)
        # mcell2 = rnn.DropoutWrapper(rnn.MultiRNNCell([rnn.DropoutWrapper(rnn.GRUCell(embedding_dim), 0.75) for _ in range(memory_dim)], state_is_tuple=True), 0.75)

        # with tf.variable_scope('decoder', reuse=True) as scope:
        self.dec_outputs, self.dec_memory = seq2seq.embedding_attention_seq2seq(self.enc_inp, self.dec_inp, mcell, num_encoder_symbols=vocab_size, num_decoder_symbols=vocab_size,
                                                                                embedding_size=embedding_dim,feed_previous=True)
        # dec_outputs_test, dec_memory_test = seq2seq.embedding_rnn_seq2seq(enc_inp, dec_inp_test, mcell, vocab_size, vocab_size, embedding_dim)

        self.loss = seq2seq.sequence_loss(self.dec_outputs, self.labels, self.weights)

        self.trainer = tf.train.AdamOptimizer(learning_rate=0.5).minimize(self.loss)

        self.saver = tf.train.Saver(tf.global_variables())

        if not os.path.exists("checkpoints"):
            os.mkdir("checkpoints")

    def save(self, sess):
            self.saver.save(sess, 'checkpoints/rnn_train.chk')

    def load(self, sess):
        my_file = Path("checkpoints/rnn_train.chk")
        if my_file.exists():
            self.saver.restore(sess, "checkpoints/rnn_train.chk")
        else:
            sess.run(tf.global_variables_initializer())


